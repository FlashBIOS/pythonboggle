#!/usr/bin/env python

import time

boggle_grid = [
    ['s', 'o', 'c', 'k'],
    ['h', 'c', 'e', 'y'],
    ['g', 'r', 'f', 'b'],
    ['o', 'l', 'l', 'a'],
]


def create_word_trie(words):
    word_trie = {}
    for word in words:
        current_trie = word_trie
        for character in word:
            # add either the character or a dictionary
            current_trie = current_trie.setdefault(character, {})
        # the \v is the leaf
        current_trie['\v'] = '\v'
    return word_trie


def word_in_trie(search_trie, word, partial_word_ok=False):
    for character in word:
        if character not in search_trie:
            return False
        search_trie = search_trie[character]
    if partial_word_ok:
        return True
    # return False if we are not at the leaf
    return '\v' in search_trie


def words_in_grid():
    found_words = []
    for row in range(len(boggle_grid)):
        for column in range(len(boggle_grid[0])):
            search_path([(row, column)], found_words)
    return found_words


def search_path(coordinates, found_words):
    # join all the characters in the current path
    potential_word = ''.join([boggle_grid[i][n] for (i, n) in coordinates])

    # word hasn't already been added
    # and limit to words of two or more letters
    # and word is a complete word in trie
    if potential_word not in found_words and len(potential_word) >= 3 and word_in_trie(word_trie, potential_word):
        found_words.append(potential_word)

    # as a shorter match may eventually lead to a longer match ('to' matches, so does 'too' and 'took') check if the
    # word we are looking at is a partial word in the trie and if it isn't we can stop searching this branch
    if word_in_trie(word_trie, potential_word, True):
        # recursively search through all the neighbor letters
        coords = coordinates[-1]
        for neighbor in [(0, 1), (1, 0), (0, -1), (-1, 0), (1, 1), (-1, -1), (1, -1), (-1, 1)]:
            neighbor_coords = (coords[0] + neighbor[0], coords[1] + neighbor[1])
            if 0 <= neighbor_coords[0] < len(boggle_grid) and 0 <= neighbor_coords[1] < len(boggle_grid[0]):
                # is this a letter we've already looked at?
                if neighbor_coords not in coordinates:
                    search_path(coordinates + [neighbor_coords], found_words)
        return found_words


start_time = time.time()
word_trie = create_word_trie(open('/usr/share/dict/words').read().splitlines())
boggle_start_time = time.time()
words = words_in_grid()
end_time = time.time()

print "The following", len(words), "words of two or more letters exist in the boggle:"
print ", ".join(words)
print "Full runtime: %.2f seconds" % (end_time - start_time)
print "Boggle only time: %.2f seconds" % (end_time - boggle_start_time)